package com.sem.tamiyaresellerapp.data

import com.google.firebase.database.Exclude

data class Address (
    var key: String?                    = "",
    var name: String?                   = "",
    var buildingName: String?           = "",
    var city: String?                   = "",
    var stateProvinceRegion: String?    = "",
    var zipCode: String?                = "",
    var country: String?                = "",
    var primary: Boolean                = true
) {
    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "name" to name,
            "buildingName" to buildingName,
            "city" to city,
            "stateProvinceRegion" to stateProvinceRegion,
            "zipcode" to zipCode,
            "country" to country,
            "primary" to primary
        )
    }
}