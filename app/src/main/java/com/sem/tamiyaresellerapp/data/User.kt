package com.sem.tamiyaresellerapp.data

import com.google.firebase.database.Exclude

data class User (
    var id: String? = "",
    var name: String? = "",
    var contactNumber: String? = "",
    var address: List<String>? = emptyList()
) {
    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "uid" to id,
            "name" to name,
            "contactNumber" to contactNumber,
            "addresses" to address
        )
    }
}