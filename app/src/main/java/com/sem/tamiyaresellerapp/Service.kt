package com.sem.tamiyaresellerapp

import android.app.IntentService
import android.app.Service
import android.content.Intent

class Service: IntentService("MainService") {
    override fun onCreate() {
        super.onCreate()
        App.component?.inject(this)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return Service.START_STICKY
    }

    override fun onHandleIntent(p0: Intent?) {
    }
}