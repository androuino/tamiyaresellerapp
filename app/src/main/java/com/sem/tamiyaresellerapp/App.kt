package com.sem.tamiyaresellerapp

import android.app.Application
import android.content.res.Resources
import com.facebook.stetho.Stetho
import com.google.firebase.database.FirebaseDatabase
import com.sem.tamiyaresellerapp.di.component.ApplicationComponent
import com.sem.tamiyaresellerapp.di.component.DaggerApplicationComponent
import com.sem.tamiyaresellerapp.di.module.ApplicationModule
import com.squareup.leakcanary.LeakCanary
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        res = resources
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)

        // leak canary
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }
        LeakCanary.install(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }

        component = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        component?.inject(this)
    }

    companion object {
        var component: ApplicationComponent? = null
            private set

        var res: Resources? = null
            private set
    }
}