package com.sem.tamiyaresellerapp.ui.base

import android.support.v4.app.FragmentManager
import com.sem.tamiyaresellerapp.R
import timber.log.Timber

class FragmentHandler(private val fragmentManager: FragmentManager) {
    val currentFragment: BaseFragment?
        get() {
            if (fragmentManager.backStackEntryCount == 0) {
                return null
            }
            val currentEntry = fragmentManager.getBackStackEntryAt(fragmentManager.backStackEntryCount - 1)

            val tag = currentEntry.name
            val fragment = fragmentManager.findFragmentByTag(tag)
            return fragment as BaseFragment
        }

    fun add(fragment: BaseFragment) {
        //don't add a fragment of the same type on top of itself.
        val currentFragment = currentFragment
        if (currentFragment != null) {
            if (currentFragment.javaClass == fragment.javaClass) {
                return
            }
        }
        Timber.tag("fragment").d(fragment.title)

        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container, fragment, fragment.title)
        fragmentTransaction.addToBackStack(fragment.title)
        fragmentTransaction.commit()
    }
}