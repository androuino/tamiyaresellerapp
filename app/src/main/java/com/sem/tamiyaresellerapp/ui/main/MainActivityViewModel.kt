package com.sem.tamiyaresellerapp.ui.main

import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseUser
import com.sem.tamiyaresellerapp.App
import com.sem.tamiyaresellerapp.ui.base.BaseViewModel
import javax.inject.Inject

class MainActivityViewModel @Inject constructor() : BaseViewModel(), LifecycleObserver {
    private var firebaseUser: FirebaseUser? = null
    private var user = MutableLiveData<String>()

    init {
        App.component?.inject(this)
    }

    fun observeLoginStatus(): LiveData<Boolean> {
        return confirmationRepo.getLoginStatus
    }

    fun getUserEmailIfLoggedIn(): LiveData<String> {
        user.postValue(confirmationRepo.getEmail())
        return user
    }

    fun getEmail(): String {
        return confirmationRepo.getEmail()
    }
}