package com.sem.tamiyaresellerapp.ui.dialogs

interface Repository {
    fun signOut(status: Boolean)
    fun getEmail() : String
}