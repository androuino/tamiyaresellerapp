package com.sem.tamiyaresellerapp.ui.base

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.sem.tamiyaresellerapp.App
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.ui.dialogs.ConfirmationRepo
import javax.inject.Inject

abstract class BaseDialogFragment: DialogFragment() {
    @Inject
    lateinit var firebaseAuth: FirebaseAuth
    @Inject
    lateinit var databaseReference: DatabaseReference
    @Inject
    lateinit var confirmationRepo: ConfirmationRepo
    lateinit var firebaseUser: FirebaseUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.component?.inject(this)
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    fun snackbar(message: String) {
        val snackbar = Snackbar.make(activity!!.window.decorView.rootView, "", Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        val tv = snackbarView.findViewById(android.support.design.R.id.snackbar_text) as TextView

        when (message) {
            "reset-success" -> {
                snackbar.setText("Success! Please check your email for link")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(context!!, R.color.snackbarSuccess))
            }
            "reset-error" -> {
                snackbar.setText("Error sending the link")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(context!!, R.color.snackbarError))
            }
            "sign-in-error" -> {
                snackbar.setText("Wrong email or password combination")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(context!!, R.color.snackbarError))
            }
            "verify-account" -> {
                snackbar.setText("Please verify your email first")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(context!!, R.color.snackbarError))
            }
            "verification-error" -> {
                snackbar.setText("Error sending email verification")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(context!!, R.color.snackbarError))
            }
            "signup-success" -> {
                snackbar.setText("Success! Please check your email for verification link")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(context!!, R.color.snackbarSuccess))
            }
            "signup-error" -> {
                snackbar.setText("Error on registering new account!")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(context!!, R.color.snackbarError))
            }
            "signup-password-error" -> {
                snackbar.setText("Password did not match!")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(context!!, R.color.snackbarError))
            }
            "invalid-password" -> {
                snackbar.setText("Password should be at least 6 characters")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(context!!, R.color.snackbarError))
            }
            "already-in-use" -> {
                snackbar.setText("The email address is already in use by another account.")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(context!!, R.color.snackbarError))
            }
            else -> {

            }
        }
        snackbar.show()
    }
}