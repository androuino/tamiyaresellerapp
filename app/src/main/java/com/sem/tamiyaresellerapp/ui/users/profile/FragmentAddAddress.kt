package com.sem.tamiyaresellerapp.ui.users.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.data.Address
import com.sem.tamiyaresellerapp.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_add_address.*

class FragmentAddAddress: BaseFragment() {
    override val title: String
        get() = "add-address"

    private val country = arrayOf("Choose a country", "Philippines", "Japan", "Malaysia", "Singapore", "Thailand", "Indonesia")
    private var selectedCountry = "Choose a country"

    private lateinit var firebaseUser: FirebaseUser
    private lateinit var firebaseDatabaseReference: DatabaseReference

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_address, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_item, country)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCountry.adapter = adapter

        if (firebaseAuth.currentUser != null) {
            firebaseUser = firebaseAuth.currentUser!!
            firebaseDatabaseReference = firebaseDatabase.getReference("address").child(firebaseUser.uid)
        }

        spinnerCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedCountry = parent?.getItemAtPosition(position).toString()
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }

        btnSaveAddress.setOnClickListener {
            when {
                tietName.text!!.isEmpty()                   -> tietName.error = "Cannot be empty!"
                tietBuildingName.text!!.isEmpty()           -> tietBuildingName.error = "Cannot be empty!"
                tietCity.text!!.isEmpty()                   -> tietCity.error = "Cannot be empty!"
                tietStateProvinceRegion.text!!.isEmpty()    -> tietStateProvinceRegion.error = "Cannot be empty!"
                tietZipCode.text!!.isEmpty()                -> tietZipCode.error = "Cannot be empty!"
                else -> {
                    val address = Address()
                    address.key                 = firebaseDatabaseReference.push().key
                    address.name                = tietName.text.toString()
                    address.buildingName        = tietBuildingName.text.toString()
                    address.city                = tietCity.text.toString()
                    address.stateProvinceRegion = tietStateProvinceRegion.text.toString()
                    address.zipCode             = tietZipCode.text.toString()
                    address.primary             = false

                    if (selectedCountry != "Choose a country")
                        address.country = selectedCountry
                    else
                        snackbar("empty")

                    firebaseDatabaseReference.child(address.key!!).setValue(address).addOnCompleteListener {
                        if (it.isSuccessful) {
                            tietName.setText("")
                            tietBuildingName.setText("")
                            tietCity.setText("")
                            tietStateProvinceRegion.setText("")
                            tietZipCode.setText("")
                            spinnerCountry.setSelection(0)
                            btnSaveAddress.requestFocus()
                        } else {
                            snackbar("address-error")
                        }
                    }
                }
            }
        }
    }

    companion object {
        fun newInstance(): FragmentAddAddress {
            return FragmentAddAddress()
        }
    }
}