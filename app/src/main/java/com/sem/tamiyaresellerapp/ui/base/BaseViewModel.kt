package com.sem.tamiyaresellerapp.ui.base

import android.arch.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.sem.tamiyaresellerapp.App
import com.sem.tamiyaresellerapp.ui.dialogs.ConfirmationRepo
import javax.inject.Inject

open class BaseViewModel: ViewModel() {
    @Inject
    lateinit var firebaseAuth: FirebaseAuth
    @Inject
    lateinit var confirmationRepo: ConfirmationRepo

    init {
        App.component?.inject(this)
    }
}