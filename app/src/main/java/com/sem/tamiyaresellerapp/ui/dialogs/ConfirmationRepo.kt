package com.sem.tamiyaresellerapp.ui.dialogs

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConfirmationRepo @Inject constructor(val firebaseAuth: FirebaseAuth): Repository {
    private lateinit var firebaseUser: FirebaseUser

    private val statOfUser = MutableLiveData<Boolean>()
    private var userEmail = ""

    val getLoginStatus: LiveData<Boolean>
        get() = statOfUser

    override fun signOut(status: Boolean) {
        statOfUser.postValue(status)
    }

    override fun getEmail() : String {
        if (firebaseAuth.currentUser != null) {
            firebaseUser = firebaseAuth.currentUser!!
            userEmail = firebaseUser.email!!
        } else {
            userEmail = ""
        }
        return userEmail
    }
}