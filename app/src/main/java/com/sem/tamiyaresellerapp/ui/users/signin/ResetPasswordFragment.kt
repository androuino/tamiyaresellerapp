package com.sem.tamiyaresellerapp.ui.users.signin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_reset_password.*
import timber.log.Timber
class ResetPasswordFragment: BaseFragment() {
    override val title: String
        get() = "reset-password"
    private var email: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_reset_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnPasswordReset.setOnClickListener {
            if (tietResetPassword.text!!.isEmpty()) {
                tietResetPassword.error = "Should not be empty!"
            } else {
                email = tietResetPassword.text.toString().trim()
                Timber.tag("reset").d(email)
                firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Timber.tag("reset").d("success!")
                        snackbar("reset-success")
                        tietResetPassword.hint
                        //navigationController.navigateToSignInFragment(0)
                    } else {
                        Timber.tag("reset").d("error!")
                        snackbar("reset-error")
                    }
                }
            }
        }
    }

    companion object {
        fun newInstance(): ResetPasswordFragment {
            return ResetPasswordFragment()
        }
    }
}