package com.sem.tamiyaresellerapp.ui.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import com.sem.tamiyaresellerapp.App
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.ui.base.BaseDialogFragment

class FragmentConfirmationDialog : BaseDialogFragment() {
    private lateinit var btnConfirm: Button
    private lateinit var btnCancel: Button
    private lateinit var tvMessage: TextView
    private lateinit var progressBar: ProgressBar

    private var title = ""
    private var mode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.component?.inject(this)
        title = arguments?.getString("title")!!
        mode = arguments?.getString("mode")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_confirmation_dialog, container, false)
        btnConfirm  = view.findViewById(R.id.btnConfirm)
        btnCancel   = view.findViewById(R.id.btnCancel)
        tvMessage   = view.findViewById(R.id.tvMessage)
        progressBar = view.findViewById(R.id.confirmationProgressBar)
        progressBar.visibility = View.INVISIBLE

        tvMessage.text = title

        btnConfirm.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            when (mode) {
                "sign_out_confirmed" -> {
                    firebaseAuth.signOut()
                    confirmationRepo.signOut(true)
                    progressBar.visibility = View.INVISIBLE
                }
            }
            dismiss()
        }

        btnCancel.setOnClickListener {
            dismiss()
        }

        return view
    }

    companion object {
        fun newInstance(title: String, mode: String): DialogFragment {
            val fragment = FragmentConfirmationDialog()
            val args = Bundle()
            args.putString("title", title)
            args.putString("mode", mode)
            fragment.arguments = args
            return fragment
        }
    }
}