package com.sem.tamiyaresellerapp.ui.main

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import com.sem.tamiyaresellerapp.App
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.Service
import com.sem.tamiyaresellerapp.ui.base.BaseActivity
import com.sem.tamiyaresellerapp.ui.dialogs.FragmentConfirmationDialog
import com.sem.tamiyaresellerapp.ui.users.signin.SignInFragment
import com.sem.tamiyaresellerapp.ui.users.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), LifecycleOwner {
    override var drawerToggle: ActionBarDrawerToggle? = null
        private set

    private lateinit var service: Intent

    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.component?.inject(this)

        viewModel.getUserEmailIfLoggedIn().observeForever { it ->
            if (it == "") {
                tvToolbarSubTitle.text = ""
                setupNavigationItems("Sign In")
            } else {
                tvToolbarSubTitle.text = it
                setupNavigationItems("Sign Out")
            }
        }
        viewModel.observeLoginStatus().observeForever { aBoolean ->
            // logged out is true, false for logged in
            if (aBoolean!!) {
                tvToolbarSubTitle.text = ""
                setupNavigationItems("Sign In")
            } else {
                tvToolbarSubTitle.text = viewModel.getEmail()
                setupNavigationItems("Sign Out")
            }
        }

        service = Intent(this, Service::class.java)
        this.startService(service)

        setupDrawerAndToggle()
        add(MainFragment.newInstance())
        supportActionBar!!.title = ""
    }

    private fun setupNavigationItems(status: String) {
        val navigationItems = arrayOf("Home", "Account", "Settings", status)
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, navigationItems)
        mainNavigation.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun setupDrawerAndToggle() {
        setSupportActionBar(mainToolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        drawerToggle = object : ActionBarDrawerToggle(this, mainDrawer, mainToolbar, 0, 0) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                isDrawerIndicatorEnabled = true
            }

            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
            }
        }
        mainDrawer!!.addDrawerListener(drawerToggle!!)
        drawerToggle!!.syncState()

        mainNavigation.setOnItemClickListener { parent, _, position, _ ->
            when (position) {
                0 -> add(MainFragment.newInstance())
                1 -> {
                    if (firebaseAuth.currentUser != null)
                        add(ProfileFragment.newInstance())
                    else
                        snackbar("not-logged-in")
                }
                2 -> {}
                3 -> {
                    if (parent.adapter.getItem(position).toString() == "Sign In")
                        add(SignInFragment.newInstance())
                    else {
                        val confirmationDialog = FragmentConfirmationDialog.newInstance("confirmation", "sign_out_confirmed")
                        confirmationDialog.show(supportFragmentManager, "confirmation")
                    }
                }
                else -> {
                }
            }
            mainDrawer!!.closeDrawer(mainNavigation)
        }
    }

    override fun onDestroy() {
        this.stopService(service)
        super.onDestroy()
    }
}