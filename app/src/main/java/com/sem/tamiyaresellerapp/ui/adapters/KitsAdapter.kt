package com.sem.tamiyaresellerapp.ui.adapters

import android.os.Handler
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sem.tamiyaresellerapp.R
import com.viewpagerindicator.CirclePageIndicator
import java.util.*

class KitsAdapter internal constructor(private val urls: Array<String>): RecyclerView.Adapter<KitsAdapter.ViewHolder>() {
    private var numPages = 0
    private var currentPage = 0
    private lateinit var mView: View

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.kits_layout, parent, false)
        mView = view
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 1
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.viewPager.adapter = SlidingImageAdapter(mView.context, urls)
        holder.circlePageIndicator.bringToFront()
        holder.circlePageIndicator.setViewPager(holder.viewPager)

        val density = mView.context.resources.displayMetrics.density

        // set circle indicator radius
        holder.circlePageIndicator.radius = (5 * density)

        numPages = urls.size

        // auto start of viewpager
        val handler = Handler()
        val update = Runnable {
            if (currentPage == numPages) {
                currentPage = 0
            }
            holder.viewPager.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 3000, 3000)

        // pager listener over indicator
        holder.circlePageIndicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                currentPage = position
            }
            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) { }
            override fun onPageScrollStateChanged(pos: Int) { }
        })
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val viewPager: ViewPager = itemView.findViewById(R.id.viewPager) as ViewPager
        val circlePageIndicator: CirclePageIndicator = itemView.findViewById(R.id.circleIndicator) as CirclePageIndicator

        override fun onClick(view: View?) {

        }
    }
}