package com.sem.tamiyaresellerapp.ui.products.kits

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.ui.adapters.KitsAdapter
import kotlinx.android.synthetic.main.fragment_kits.*

class KitsFragment: Fragment() {
    private lateinit var urls: Array<String>
    private lateinit var kitsAdapter: KitsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_kits, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        assert(arguments != null)
        urls = arguments!!["urls"] as Array<String>

        kitsAdapter = KitsAdapter(urls)
        kitsRV.adapter = kitsAdapter
        kitsRV.layoutManager = LinearLayoutManager(activity)
        kitsAdapter.notifyDataSetChanged()
    }

    companion object {
        fun newInstance(urls: Array<String>): KitsFragment {
            val args = Bundle()
            args.putCharSequenceArray("urls", urls)
            val fragment = KitsFragment()
            fragment.arguments = args
            return fragment
        }
    }
}