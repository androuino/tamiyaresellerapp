package com.sem.tamiyaresellerapp.ui.users.profile

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.data.Address
import com.sem.tamiyaresellerapp.ui.adapters.AddressAdapter
import com.sem.tamiyaresellerapp.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_address_book.*
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FragmentAddressBook: BaseFragment() {
    override val title: String
        get() = "address-book"

    private lateinit var firebaseUser: FirebaseUser
    private lateinit var firebaseDatabaseReference: DatabaseReference
    private var listener: ValueEventListener? = null
    private lateinit var addressAdapter: AddressAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_address_book, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (firebaseAuth.currentUser != null) {
            firebaseUser = firebaseAuth.currentUser!!
            firebaseDatabaseReference = firebaseDatabase.getReference("address").child(firebaseUser.uid)

            rvAddressBook.layoutManager = LinearLayoutManager(activity)
            getListOfAddresses()
        }

        btnAddNewAddress.setOnClickListener {
            add(FragmentAddAddress.newInstance())
        }
    }

    private fun getListOfAddresses() {
        val listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val addresses = arrayListOf<Address>()
                val keys: MutableList<String> = mutableListOf()

                if (dataSnapshot.value != null) {
                    dataSnapshot.children.forEach {
                        val address: Address = it.getValue(Address::class.java)!!
                        Timber.tag("addresses").d(it.key)
                        addresses.add(address)
                        keys.add(it.key!!)
                    }
                    addresses.forEach {
                        Timber.tag("addresses").d(it.city)
                        Timber.tag("addresses").d(it.stateProvinceRegion)
                        Timber.tag("addresses").d(it.zipCode)
                        Timber.tag("addresses").d(it.buildingName)
                        Timber.tag("addresses").d(it.country)
                        Timber.tag("addresses").d("${it.primary}")
                    }
                    if (rvAddressBook != null) {
                        addressAdapter = AddressAdapter(addresses)
                        rvAddressBook.adapter = addressAdapter
                        addressAdapter.notifyDataSetChanged()

                        // radio button click listener
                        addressAdapter.onItemClick = { position ->
                            keys.forEachIndexed { index, s ->
                                if (index == position) {
                                    val address: Address = addresses[position]
                                    address.primary = true
                                    firebaseDatabaseReference.child(s).setValue(address)
                                } else {
                                    val address: Address = addresses[index]
                                    address.primary = false
                                    firebaseDatabaseReference.child(s).setValue(address)
                                }
                            }
                        }

                        // button for editing click listener
                        addressAdapter.onItemClick = { position ->
                            val address: Address = addresses[position]
                            add(FragmentAddAddress.newInstance())
                        }
                    }
                }
            }

            override fun onCancelled(dbError: DatabaseError) {
                Timber.tag("addresses").d(dbError.message)
            }
        }
        firebaseDatabaseReference.addValueEventListener(listener)
        this.listener = listener
    }

    override fun onStop() {
        super.onStop()
        listener?.let {
            databaseReference.removeEventListener(it)
        }
    }

    companion object {
        fun newInstance(): FragmentAddressBook {
            return FragmentAddressBook()
        }
    }
}