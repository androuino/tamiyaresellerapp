package com.sem.tamiyaresellerapp.ui.adapters

import android.content.Context
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.sem.tamiyaresellerapp.R

class SlidingImageAdapter(context: Context, urls: Array<String>): PagerAdapter() {
    private var urls: Array<String>? = null
    private var context: Context? = null
    private var inflater: LayoutInflater? = null

    init {
        this.urls = urls
        this.context = context
        this.inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun getCount(): Int {
        return urls!!.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageLayout = inflater!!.inflate(R.layout.sliding_images_layout, container, false)!!
        val imageView = imageLayout.findViewById(R.id.imageView) as ImageView

        Glide.with(this.context!!)
            .load(urls!![position])
            .into(imageView)

        container.addView(imageLayout, 0)

        return imageLayout
    }

    override fun saveState(): Parcelable? {
        return null
    }
}