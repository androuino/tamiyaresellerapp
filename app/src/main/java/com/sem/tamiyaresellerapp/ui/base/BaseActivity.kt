package com.sem.tamiyaresellerapp.ui.base

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.sem.tamiyaresellerapp.App
import com.sem.tamiyaresellerapp.R
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {
    @Inject
    lateinit var firebaseAuth: FirebaseAuth
    lateinit var firebaseUser: FirebaseUser

    private var fragmentManager: FragmentManager? = null
    private lateinit var fragmentHandler: FragmentHandler

    private val navigationBackPressListener = View.OnClickListener { fragmentManager!!.popBackStack() }
    private val backStackListener = FragmentManager.OnBackStackChangedListener { onBackStackChangedEvent() }

    protected abstract val drawerToggle: ActionBarDrawerToggle?

    private fun onBackStackChangedEvent() {
        syncDrawerToggleState()
    }

    private fun syncDrawerToggleState() {
        val drawerToggle = drawerToggle ?: return
        if (fragmentManager!!.backStackEntryCount > 1) {
            drawerToggle.isDrawerIndicatorEnabled = false
            drawerToggle.toolbarNavigationClickListener = navigationBackPressListener //popbackstack
            mainDrawer.hideKeyboard()
        } else {
            drawerToggle.isDrawerIndicatorEnabled = true
            drawerToggle.toolbarNavigationClickListener = drawerToggle.toolbarNavigationClickListener //open nav menu drawer
            mainDrawer.hideKeyboard()
        }
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.component?.inject(this)
        fragmentManager = supportFragmentManager
        fragmentHandler = FragmentHandler(fragmentManager!!)
        fragmentManager!!.addOnBackStackChangedListener(backStackListener)
    }

    override fun onResume() {
        super.onResume()
        mainDrawer!!.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

            }

            override fun onDrawerOpened(drawerView: View) {
                syncDrawerToggleState()
                drawerView.hideKeyboard()
            }

            override fun onDrawerClosed(drawerView: View) {
                syncDrawerToggleState()
                drawerView.hideKeyboard()
            }

            override fun onDrawerStateChanged(newState: Int) {

            }
        })
    }

    override fun onDestroy() {
        fragmentManager!!.removeOnBackStackChangedListener(backStackListener)
        fragmentManager = null
        super.onDestroy()
    }

    protected fun add(fragment: BaseFragment) {
        fragmentHandler.add(fragment)
    }

    override fun onBackPressed() {
        if (sendBackPressToDrawer()) {
            //the drawer consumed the backpressed
            return
        }

        if (sendBackPressToFragmentOnTop()) {
            // fragment on top consumed the back press
            return
        }

        //let the android system handle the back press, usually by popping the fragment
        super.onBackPressed()

        //close the activity if back is pressed on the root fragment
        if (fragmentManager!!.backStackEntryCount == 0) {
            finish()
        }
    }

    private fun sendBackPressToDrawer(): Boolean {
        val drawer = mainDrawer
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
            return true
        }
        return false
    }

    fun View.hideKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun sendBackPressToFragmentOnTop(): Boolean {
        val fragmentOnTop = fragmentHandler.currentFragment ?: return false
        return if (fragmentOnTop !is BackButtonSupportFragment) {
            false
        } else (fragmentOnTop as BackButtonSupportFragment).onBackPressed()
    }

    fun snackbar(message: String) {
        val snackbar = Snackbar.make(this.window.decorView.rootView, "", Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        val tv = snackbarView.findViewById(android.support.design.R.id.snackbar_text) as TextView

        when (message) {
            "reset-success" -> {
                snackbar.setText("Success! Please check your email for link")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarSuccess))
            }
            "reset-error" -> {
                snackbar.setText("Error sending the link")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarError))
            }
            "sign-in-error" -> {
                snackbar.setText("Wrong email or password combination")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarError))
            }
            "verify-account" -> {
                snackbar.setText("Please verify your email first")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarError))
            }
            "verification-error" -> {
                snackbar.setText("Error sending email verification")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarError))
            }
            "signup-success" -> {
                snackbar.setText("Success! Please check your email for verification link")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarSuccess))
            }
            "signup-error" -> {
                snackbar.setText("Error on registering new account!")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarError))
            }
            "signup-password-error" -> {
                snackbar.setText("Password did not match!")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarError))
            }
            "invalid-password" -> {
                snackbar.setText("Password should be at least 6 characters")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarError))
            }
            "already-in-use" -> {
                snackbar.setText("The email address is already in use by another account.")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarError))
            }
            "not-logged-in" -> {
                snackbar.setText("You are not currently logged in.")
                tv.setTextColor(Color.WHITE)
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbarError))
            }
            else -> {

            }
        }
        snackbar.show()
    }
}