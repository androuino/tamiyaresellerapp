package com.sem.tamiyaresellerapp.ui.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.google.firebase.database.*
import com.sem.tamiyaresellerapp.App
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.data.User
import com.sem.tamiyaresellerapp.ui.base.BaseDialogFragment
import kotlinx.android.synthetic.main.fragment_edit_name_dialog.*
import timber.log.Timber

class FragmentEditNameDialog : BaseDialogFragment() {
    private var userListener: ValueEventListener? = null

    private lateinit var etEditNameNumber: EditText

    private var title = ""
    private var mode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.component?.inject(this)
        title = arguments?.getString("name")!!
        mode = arguments?.getString("mode")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_name_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        etEditNameNumber = view.findViewById(R.id.etEditNameNumber)

        when (mode) {
            "name" -> {
                tvTitle.text = getString(R.string.profile_name)
                etEditNameNumber.setText(title)
                etEditNameNumber.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS
            }
            "number" -> {
                tvTitle.text = getString(R.string.profile_number)
                etEditNameNumber.setText(title)
                etEditNameNumber.inputType = InputType.TYPE_CLASS_NUMBER
            }
            else -> {}
        }

        if (firebaseAuth.currentUser != null) {
            firebaseUser = firebaseAuth.currentUser!!
            getUserUpdatedInfo()
        }

        btnSave.setOnClickListener {
            update()
            btnSave.hideKeyboard()
            dismiss()
        }
    }

    private fun update() {
        when (mode) {
            "name" -> {
                pushUserName(firebaseUser.uid, etEditNameNumber.text.toString())
            }
            "number" -> {
                pushUserContactNumber(firebaseUser.uid, etEditNameNumber.text.toString())
            }
            else -> {}
        }
    }

    private fun pushUserName(userId: String, name: String) {
        databaseReference.push().key
        databaseReference.child("users").child(userId).child("name").setValue(name)
    }

    private fun pushUserContactNumber(userId: String, contactNumber: String) {
        databaseReference.push().key
        databaseReference.child("users").child(userId).child("contactNumber").setValue(contactNumber)
    }

    private fun getUserUpdatedInfo() {
        val userListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.value != null) {
                    val userData = dataSnapshot.getValue(User::class.java)
                    when (mode) {
                        "name" -> {
                            etEditNameNumber.setText(userData?.name)
                        }
                        "number" -> {
                            etEditNameNumber.setText(userData?.contactNumber!!.toString())
                            //Timber.tag("data").d(userData?.contactNumber!!.toString())
                        }
                        else -> {}
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Timber.tag("databaseError").d(databaseError.toException())
            }
        }
        databaseReference.child("users").child(firebaseUser.uid).addValueEventListener(userListener)

        this.userListener = userListener
    }

    override fun onDestroy() {
        super.onDestroy()
        userListener?.let {
            databaseReference.removeEventListener(it)
        }
    }

    companion object {
        fun newInstance(name: String, mode: String): DialogFragment {
            val fragment = FragmentEditNameDialog()
            val args = Bundle()
            args.putString("name", name)
            args.putString("mode", mode)
            fragment.arguments = args
            return fragment
        }
    }
}