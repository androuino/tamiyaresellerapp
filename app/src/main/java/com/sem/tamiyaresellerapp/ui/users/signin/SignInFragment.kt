package com.sem.tamiyaresellerapp.ui.users.signin

import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.google.firebase.auth.FirebaseUser
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.ui.base.BaseFragment
import com.sem.tamiyaresellerapp.ui.users.signup.SignUpFragment
import kotlinx.android.synthetic.main.fragment_sign_in.*
import timber.log.Timber

class SignInFragment: BaseFragment() {
    override val title: String
        get() = "signin"

    private lateinit var progressBar: ProgressBar

    private var email: String = ""
    private var password: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.progressBar)
        progressBar.visibility = View.INVISIBLE

        etSignInPassword.transformationMethod = PasswordTransformationMethod()

        btnLogin.setOnClickListener {
            login()
            btnLogin.hideKeyboard()
        }

        btnRegister.setOnClickListener {
            add(SignUpFragment.newInstance())
            btnRegister.hideKeyboard()
        }

        tvForgotPassword.setOnClickListener {
            add(ResetPasswordFragment.newInstance())
        }
    }

    private fun login() {
        progressBar.visibility = View.VISIBLE
        when {
            etSigninEmail.text.isEmpty() -> etSigninEmail.error = "Should not be empty!"
            etSignInPassword.text!!.isEmpty() -> etSignInPassword.error = "Should not be empty!"
            else -> {
                email = etSigninEmail.text.toString()
                password = etSignInPassword.text.toString()

                firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
                    if (it.isSuccessful) {
                        checkIfEmailIsVerified()
                        progressBar.visibility = View.INVISIBLE
                    } else {
                        Timber.tag("login").d("login error!")
                        snackbar("sign-in-error")
                        progressBar.visibility = View.INVISIBLE
                    }
                }
            }
        }
    }

    private fun checkIfEmailIsVerified() {
        val user: FirebaseUser = firebaseAuth.currentUser!!

        if (user.isEmailVerified) {
            Timber.tag("login").d("Successful!")
            confirmationRepo.signOut(false)
            activity?.onBackPressed()
        } else {
            snackbar("verify-account")
            firebaseAuth.signOut()
        }
    }

    companion object {
        fun newInstance(): SignInFragment {
            return SignInFragment()
        }
    }
}