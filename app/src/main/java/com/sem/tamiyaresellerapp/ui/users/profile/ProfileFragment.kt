package com.sem.tamiyaresellerapp.ui.users.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.data.User
import com.sem.tamiyaresellerapp.ui.base.BaseFragment
import com.sem.tamiyaresellerapp.ui.dialogs.FragmentEditNameDialog
import com.sem.tamiyaresellerapp.ui.users.signin.ResetPasswordFragment
import kotlinx.android.synthetic.main.fragment_profile.*
import timber.log.Timber

class ProfileFragment: BaseFragment() {
    override val title: String
        get() = "profile"

    private lateinit var firebaseUser: FirebaseUser
    private var userListener: ValueEventListener? = null

    private lateinit var profileProgressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvEditPassword.text = "********"
        profileProgressBar = view.findViewById(R.id.profileProgressBar)
        profileProgressBar.visibility = View.INVISIBLE

        if (firebaseAuth.currentUser != null) {
            firebaseUser = firebaseAuth.currentUser!!
            tvEditEmail.text = firebaseUser.email
            getFirebaseUserInfo()
        }

        layoutProfileName.setOnClickListener {
            val editNameDialog = FragmentEditNameDialog.newInstance(tvEditName.text.toString(), "name")
            editNameDialog.show(activity?.supportFragmentManager, "edit-name")
        }

        layoutProfileContactNumber.setOnClickListener {
            val editNameDialog = FragmentEditNameDialog.newInstance(tvEditContactNumber.text.toString(), "number")
            editNameDialog.show(activity?.supportFragmentManager, "edit-contact-number")
        }

        layoutProfileAddress.setOnClickListener {
            add(FragmentAddressBook.newInstance())
        }

        layoutProfilePassword.setOnClickListener {
            add(ResetPasswordFragment.newInstance())
        }
    }

    private fun getFirebaseUserInfo() {
        profileProgressBar.visibility = View.VISIBLE
        val userListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.value != null) {
                    val userData = dataSnapshot.getValue(User::class.java)
                    tvEditName.text = userData?.name
                    tvEditContactNumber.text = userData?.contactNumber.toString()
                }
                profileProgressBar.visibility = View.INVISIBLE
                /*for (ds in dataSnapshot.children) {
                    tvEditName.text = ds.child("name").getValue(String::class.java)
                    tvEditContactNumber.text = ds.child("contactNumber").getValue(Integer::class.java).toString()
                    Timber.tag("data").d(ds.child("id").getValue(String::class.java))
                    Timber.tag("data").d(ds.child("name").getValue(String::class.java))
                    Timber.tag("data").d(ds.child("contactNumber").getValue(Integer::class.java).toString())
                    Timber.tag("data").d(ds.child("address").getValue(String::class.java))
                }*/
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Timber.tag("databaseError").d(databaseError.toException())
                profileProgressBar.visibility = View.INVISIBLE
            }
        }
        databaseReference.child("users").child(firebaseUser.uid).addValueEventListener(userListener)
        this.userListener = userListener
    }

    override fun onStop() {
        super.onStop()
        userListener?.let {
            databaseReference.removeEventListener(it)
        }
    }

    companion object {
        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }
}