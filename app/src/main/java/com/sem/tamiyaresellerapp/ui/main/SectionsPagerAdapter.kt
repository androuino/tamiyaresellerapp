package com.sem.tamiyaresellerapp.ui.main

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.ui.products.accessories.AccessoriesFragment
import com.sem.tamiyaresellerapp.ui.products.containers.ContainersFragment
import com.sem.tamiyaresellerapp.ui.products.kits.KitsFragment
import timber.log.Timber

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2,
    R.string.tab_text_3
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private val urls = arrayOf(
        "https://demonuts.com/Demonuts/SampleImages/W-03.JPG",
        "https://demonuts.com/Demonuts/SampleImages/W-08.JPG",
        "https://demonuts.com/Demonuts/SampleImages/W-10.JPG",
        "https://demonuts.com/Demonuts/SampleImages/W-13.JPG",
        "https://demonuts.com/Demonuts/SampleImages/W-17.JPG",
        "https://demonuts.com/Demonuts/SampleImages/W-21.JPG"
    )

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Timber.tag("position").d("$position")
        //return PlaceholderFragment.newInstance(position)
        return when (position) {
            0 -> KitsFragment.newInstance(urls)
            1 -> AccessoriesFragment()
            else -> ContainersFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 3 total pages.
        return 3
    }
}