package com.sem.tamiyaresellerapp.ui.users.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.google.firebase.auth.FirebaseUser
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_up.*
import timber.log.Timber

class SignUpFragment: BaseFragment() {
    override val title: String
        get() = "signup"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBarSignUp.visibility = View.INVISIBLE

        btnSignUp.setOnClickListener {
            register(etEmail.text.toString(), etSignUpPassword.text.toString())
            btnSignUp.hideKeyboard()
        }
    }

    private fun register(email: String, password: String) {
        progressBarSignUp.visibility = View.VISIBLE
        when {
            etEmail.text.isEmpty() -> etEmail.error = "Should not be empty!"
            etSignUpPassword.text!!.isEmpty() -> etSignUpPassword.error = "Should not be empty!"
            etConfirmPassword.text!!.isEmpty() -> etConfirmPassword.error = "Should not be empty!"
            else -> {
                if (etSignUpPassword.text.toString() != etConfirmPassword.text.toString()) {
                    snackbar("signup-password-error")
                } else {
                    firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener {
                        if (it.isSuccessful) {
                            sendEmailVerification(email)
                            progressBarSignUp.visibility = View.INVISIBLE
                        } else {
                            Timber.tag("signup").d(it.exception?.message)
                            when {
                                it.exception?.message!!.contains("invalid") -> snackbar("invalid-password")
                                it.exception?.message!!.contains("already") -> snackbar("already-in-use")
                                else -> snackbar("signup-error")
                            }
                            progressBarSignUp.visibility = View.INVISIBLE
                        }
                    }
                }
            }
        }
    }

    private fun sendEmailVerification(email: String) {
        val user: FirebaseUser = firebaseAuth.currentUser!!
        user.sendEmailVerification().addOnCompleteListener {
            if (it.isSuccessful) {
                snackbar("signup-success")
                firebaseAuth.signOut()
                activity?.onBackPressed()
            } else {
                snackbar("verification-error")
                Timber.tag("verification").d(it.exception)
                firebaseAuth.signOut()
                activity?.onBackPressed()
            }
        }
    }

    companion object {
        fun newInstance(): SignUpFragment {
            return SignUpFragment()
        }
    }
}