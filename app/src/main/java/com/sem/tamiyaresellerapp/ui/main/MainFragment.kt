package com.sem.tamiyaresellerapp.ui.main

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.*
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment: BaseFragment() {
    override var title: String = ""
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        val args = arguments
        title = args!!.getSerializable(TITLE) as String
        if (title == "" || title.isEmpty()) {
            throw IllegalArgumentException("Must supply a book title") as Throwable
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sectionsPagerAdapter = SectionsPagerAdapter(activity!!.baseContext, childFragmentManager)
        view_pager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(view_pager)

        fab.setOnClickListener {
            Snackbar.make(view, "Your basket", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    companion object {
        private const val TITLE = "main"

        fun newInstance(title: String = "main"): MainFragment {
            val mainFragment = MainFragment()
            val args = Bundle()
            args.putSerializable(TITLE, title)
            mainFragment.arguments = args
            return mainFragment
        }
    }
}