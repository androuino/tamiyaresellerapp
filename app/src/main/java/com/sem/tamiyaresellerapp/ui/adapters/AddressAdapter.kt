package com.sem.tamiyaresellerapp.ui.adapters

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.TextView
import com.sem.tamiyaresellerapp.R
import com.sem.tamiyaresellerapp.data.Address

class AddressAdapter internal constructor(private val addressList: ArrayList<Address>): RecyclerView.Adapter<AddressAdapter.ViewHolder>() {
    var onItemClick: (Int) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): AddressAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_address, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return addressList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val addresses = addressList[position]
        val receiverName = addresses.name
        val text = addresses.city + "\n" +
                addresses.stateProvinceRegion + "\n" +
                addresses.zipCode + "\n" +
                addresses.buildingName + "\n" +
                addresses.country
        holder.tvReceiverName.text = receiverName
        holder.multiTextAddress.text = text

        holder.rbPrimaryAddress.isChecked = addresses.primary

        if (holder.rbPrimaryAddress.isChecked) {
            holder.layoutAddressBook.setBackgroundResource(R.drawable.bg_primary_address)
        }

        holder.rbPrimaryAddress.setOnClickListener {
            onItemClick(position)
            notifyDataSetChanged()
        }
        holder.btnEditAddress.setOnClickListener {
            onItemClick(position)
        }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val layoutAddressBook: ConstraintLayout = itemView.findViewById(R.id.layoutAddressBook) as ConstraintLayout
        val tvReceiverName: TextView            = itemView.findViewById(R.id.tvReceiverName) as TextView
        val multiTextAddress: TextView          = itemView.findViewById(R.id.tvAddress) as TextView
        val rbPrimaryAddress: RadioButton       = itemView.findViewById(R.id.rbPrimaryAddress) as RadioButton
        val btnEditAddress: Button              = itemView.findViewById(R.id.btnEditAddress) as Button

        override fun onClick(view: View?) {
        }
    }
}