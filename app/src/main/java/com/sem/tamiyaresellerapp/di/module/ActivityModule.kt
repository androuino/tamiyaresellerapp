package com.sem.tamiyaresellerapp.di.module

import android.app.Activity
import android.content.Context
import com.sem.tamiyaresellerapp.ui.main.MainActivity
import com.sem.tamiyaresellerapp.di.ActivityContext
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {
    @Provides
    @ActivityContext
    internal fun provideContext(): Context {
        return activity
    }

    @Provides
    internal fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun provideMainActivity(): MainActivity {
        return MainActivity()
    }
}