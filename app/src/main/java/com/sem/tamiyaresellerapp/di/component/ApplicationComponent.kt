package com.sem.tamiyaresellerapp.di.component

import android.app.Application
import android.content.Context
import com.sem.tamiyaresellerapp.App
import com.sem.tamiyaresellerapp.ui.main.MainActivity
import com.sem.tamiyaresellerapp.Service
import com.sem.tamiyaresellerapp.di.ApplicationContext
import com.sem.tamiyaresellerapp.di.module.ApplicationModule
import com.sem.tamiyaresellerapp.di.module.FragmentModule
import com.sem.tamiyaresellerapp.di.module.ViewModelModule
import com.sem.tamiyaresellerapp.ui.base.BaseActivity
import com.sem.tamiyaresellerapp.ui.base.BaseDialogFragment
import com.sem.tamiyaresellerapp.ui.base.BaseFragment
import com.sem.tamiyaresellerapp.ui.base.BaseViewModel
import com.sem.tamiyaresellerapp.ui.dialogs.FragmentConfirmationDialog
import com.sem.tamiyaresellerapp.ui.dialogs.FragmentEditNameDialog
import com.sem.tamiyaresellerapp.ui.users.signin.ResetPasswordFragment
import com.sem.tamiyaresellerapp.ui.users.signin.SignInFragment
import com.sem.tamiyaresellerapp.ui.users.signup.SignUpFragment
import com.sem.tamiyaresellerapp.ui.users.profile.ProfileFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    ViewModelModule::class,
    FragmentModule::class
])
interface ApplicationComponent {

    @get:ApplicationContext
    val context: Context

    val application: Application

    // application
    fun inject(app: App)

    // activities
    fun inject(baseActivity: BaseActivity)
    fun inject(mainActivity: MainActivity)

    // fragments
    fun inject(baseFragment: BaseFragment)
    fun inject(signInFragment: SignInFragment)
    fun inject(signUpFragment: SignUpFragment)
    fun inject(profileFragment: ProfileFragment)
    fun inject(resetPasswordFragment: ResetPasswordFragment)

    // viewmodels
    fun inject(baseViewModel: BaseViewModel)

    // dialogs
    fun inject(baseDialogFragment: BaseDialogFragment)
    fun inject(confirmationDialog: FragmentConfirmationDialog)
    fun inject(fragmentEditNameDialog: FragmentEditNameDialog)

    // services
    fun inject(service: Service)
}